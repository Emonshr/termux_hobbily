#!/bin/fish

#This script will store the succeeded download and failed download into two different files, assigned to $S and $F.
set F ~/failed-urls.txt
set S ~/success-urls.txt
set success null
set counter null
set MostRecent null
set TargetURL null
set LastURL null
set ytURL null
set RememberLastURL null

source ~/fish/extFunc

# Just to make sure that no invalid url can get into the history
function URL_Validator
	finisher 
	
	string match -r '^http' "$TargetURL"; or \
	string match -r '^http' "$LastURL"; and \
	not test -z $TargetURL
end


function finisher
	test $RememberLastURL = "y"; \
	and set TargetURL $LastURL
end


#Main access point
function re
	while not string match -r '^[Qq]$' "$REPLY"
		
	echo -e "\nDownload most recent File[y], \nShow download history[h], \nSelect The File to resume[s]?, Quit[q]"
	
	read -n1 REPLY -P 'Choose> '

	if string match -r '^[Yy]$' "$REPLY"
	echo -e "\nResuming the most recent download..."
	set LastURL (tail -n 2 $F \
		| grep "^[0-9]" \
		| cut -d " " -f 3)
	
		if string match -r '^.*youtu.*$' "$LastURL"
			
			set ytURL $LastURL ; and \
			set RememberLastURL y; \
			and yout
			set RememberLastURL null
					
		else
		set RememberLastURL y
		aria2c --split=16 --max-connection-per-server=16 --min-split-size=1M --continue=true -d ~/storage/shared/Download/ --enable-dht=true --seed-time=0  --max-upload-limit=1K $LastURL
		Linksaver
		set RememberLastURL null		
		end
	
	else if string match -r '^[Hh]$' "$REPLY"
	echo -e "\nShowing the download history..."
	echo -e "\nFailed Download or Successful[F/S]?"
	read -n1 REPLY -P 'Choose> '
	
		if string match -r '^[Ff]$' "$REPLY"
		less $F
		else if string match -r '^[Ss]$' "$REPLY"
		less $S
		else
		echo -e "\ninvalid option!"
		continue
		end
	
	else if string match -r  '^[Ss]$' "$REPLY"
	echo -e "\nSelecting a file..."
	echo -e "\nFailed Download or Successful[F/S]?"
	read -n1 REPLY -P 'Choose> '

		if  string match -r '^[Ff]$' "$REPLY"
		set TheTargetFile $F
		else if  string match -r '^[Ss]$' "$REPLY"
		set TheTargetFile $S
		else
		echo -e "\ninvalid option!"
		continue
		end
	echo -e "\nGive the number.."
	read REPLY -P 'Choose> '
	
	set TargetURL (grep "^$REPLY " $TheTargetFile \
	| cut -d " " -f 3)
	
	
		if  string match -r '^([0-9]+)*$' "$REPLY" ; \
		and not test -z "$TargetURL"
	       
			if string match -r '^.*youtu.*$'  "$TargetURL"
			
			set ytURL $TargetURL; and yout
			
			else
			aria2c --split=16 --max-connection-per-server=16 --min-split-size=1M --continue=true -d ~/storage/shared/Download/ --enable-dht=true --seed-time=0  --max-upload-limit=1K $TargetURL
			Linksaver

			end
		
		else
		echo -e "\ninvalid character!"
		continue
		end
		
	
	else if  string match -r '^[Vv]$' "$REPLY"
		
		echo -e "\n"
		read -n1 REPLY -P 'Choose> '
		test $REPLY = "s" ; and vim $S
		test $REPLY = "f"  ; and vim $F
				
	else if  string match -r '^[Qq]$' "$REPLY"
		echo -e "\nQuitting.."
	else
	echo -e "\ninvalid option! Displaying main menu.."

	end
end
set REPLY null

end

#checking if the download is successful. After the exit status, "success" variable will be set respectively and history will be printed with proper formatting.
function Linksaver
	
	test $status -eq 0 ; and set success y ; and Mo ; and \
	URL_Validator ; and \
	printf "\n%d --> %s\n \t\t-->%s<--" (echo $counter) (echo $TargetURL) (date +"%d/%m/%y||%H:%M:%S") >> $S	
	
	test $status -ne 0 ; and set success n ; and Mo ; and \
	URL_Validator ; and \
	printf "\n%d --> %s\n \t\t-->%s<--" (echo $counter) (echo $TargetURL) (date +"%d/%m/%y||%H:%M:%S") >> $F
	
	echo -e '\a'	
	#echo -e "\n$TargetURL, $LastURL, $RememberLastURL, $success, $MostRecent"
end
