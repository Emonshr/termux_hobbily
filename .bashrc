#!/bin/bash

#This script will store the succeeded download and failed download into two different files, assigned to $S and $F.
F=~/storage/shared/Download/failed-urls.txt
S=~/storage/shared/Download/success-urls.txt
success=null
counter=null
MostRecent=null
TargetURL=null
LastURL=null
ytURL=null
RememberLastURL=null

source ~/bin/extFunc

# Just to make sure that no invalid url can get into the history
function URL_Validator () {
	finisher 
	
	[[ $TargetURL =~ ^http ]] || \
	[[ $LastURL =~ ^http ]] && \
	[ ! -z $TargetURL ]
}	


function finisher () {
	[ $RememberLastURL == "y" ] \
	&& TargetURL=$LastURL
}


#Main access point
function re() {
	while [[ ! $REPLY =~ ^[Qq]$ ]]; do
		
	echo -e "\nDownload most recent File[y], \nShow download history[h], \nSelect The File to resume[s]?, Quit[q]"
	
    read -n1 -p 'Choose> '

	if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo -e "\nResuming the most recent download..."
	LastURL=$(tail -n 2 $F \
		| grep "^[0-9]" \
		| cut -d " " -f 3)
	
		if [[ $LastURL =~ ^.*youtu.*$ ]]; then
			
			ytURL=$LastURL && \
			RememberLastURL=y \
			&& yout
			RememberLastURL=null
					
		else
		RememberLastURL=y
		aria2c --split=16 --max-connection-per-server=16 --min-split-size=1M --continue=true -d ~/storage/shared/Download/ --enable-dht=true --seed-time=0  --max-upload-limit=1K $LastURL
		Linksaver
		RememberLastURL=null		
		fi
	
	elif [[ $REPLY =~ ^[Hh]$ ]]; then
	echo -e "\nShowing the download history..."
	echo -e "\nFailed Download or Successful[F/S]?"
	read -n1 -p 'Choose> '

		if [[ $REPLY =~ ^[Ff]$ ]]; then
		less $F
		elif [[ $REPLY =~ ^[Ss]$ ]]; then
		less $S
		else
		echo -e "\ninvalid option!"
		continue
		fi
	
	elif [[ $REPLY =~ ^[Ss]$ ]]; then
	echo -e "\nSelecting a file..."
	echo -e "\nFailed Download or Successful[F/S]?"
	read -n1 -p 'Choose> '

		if [[ $REPLY =~ ^[Ff]$ ]]; then
		TheTargetFile=$F
		elif [[ $REPLY =~ ^[Ss]$ ]]; then
		TheTargetFile=$S
		else
		echo -e "\ninvalid option!"
		continue
		fi
	echo -e "\nGive the number.."
	read -p 'Choose> '
	
	TargetURL=$(grep "^$REPLY " $TheTargetFile \
	| cut -d " " -f 3)
	
	
		if [[ $REPLY =~ ^([0-9]+)*$ \
		&& ! -z $TargetURL  ]]; \
		then 
			if [[ $TargetURL =~ \
			^.*youtu.*$  ]]; 
			then
			ytURL=$TargetURL && yout
			
			else
			aria2c --split=16 --max-connection-per-server=16 --min-split-size=1M --continue=true -d ~/storage/shared/Download/ --enable-dht=true --seed-time=0  --max-upload-limit=1K $TargetURL
			Linksaver

			fi
		
		else
		echo -e "\ninvalid character!"
		continue
		fi
		
	
	elif [[ $REPLY =~ ^[Vv]$ ]]; then
		
		echo -e "\n"
		read -n1 -p 'Choose> '
		[ $REPLY == "s" ]  && vim $S
		[ $REPLY == "f" ]  && vim $F
				
	elif [[ $REPLY =~ ^[Qq]$ ]]; then
		echo -e "\nQuitting.."
	else
	echo -e "\ninvalid option! Displaying main menu.."

	fi
done
REPLY=null

}

#checking if the download is successful. After the exit status, "success" variable will be set respectively and history will be printed with proper formatting.
function Linksaver() {
	
	[ $? == "0" ] && success=y && Mo && \
	URL_Validator && \
	printf "\n%d --> %s\n \t\t-->%s<--" $(echo $counter) $(echo $TargetURL) $(date +"%d/%m/%y||%H:%M:%S") >> $S	
	
	[ $? != "0" ] && success=n && Mo && \
	URL_Validator && \
	printf "\n%d --> %s\n \t\t-->%s<--" $(echo $counter) $(echo $TargetURL) $(date +"%d/%m/%y||%H:%M:%S") >> $F
	
	echo -e '\a'	
	#echo -e "\n$TargetURL, $LastURL, $RememberLastURL, $success, $MostRecent"
} 